data "template_file" "user_data" {
  template = "${file("${path.module}/files/cloud_init.cfg")}"
  vars = {
    SSH_AUTH_KEY = var.SSH_AUTH_KEY
    USERNAME = var.USERNAME
  }
}

resource "libvirt_cloudinit_disk" "commoninit" {
  name = "${var.NAME}-commoninit.iso"
  pool = "default"
  user_data = "${data.template_file.user_data.rendered}"
}

resource "libvirt_volume" "vm_q2" {
  name = "${var.NAME}-q2vol"
  pool = "default"
  source = var.Q2_SOURCE
  format = "qcow2"
}

resource "libvirt_volume" "vm_q2_resized" {
  name = "${var.NAME}-vm_q2_resized"
  base_volume_id = libvirt_volume.vm_q2.id
  pool = "default"
  size = var.DISK_SIZE
}

resource "libvirt_domain" "libvirt-cloud-init-vm" {
  name = "${var.NAME}"
  memory = var.VM_MEMORY
  vcpu = var.VM_VCPU
  firmware = var.FIRMWARE == "" ? null : var.FIRMWARE

  cpu {
    mode = var.VM_CPU_MODE
  }

  network_interface {
    network_name = var.VM_NETWORK
    wait_for_lease = var.VM_NETWORK_WAIT_FOR_LEASE
  }

  disk {
    volume_id = "${libvirt_volume.vm_q2_resized.id}"
  }

  cloudinit = "${libvirt_cloudinit_disk.commoninit.id}"

  kernel = var.KERNEL_OVERWRITE != null ? libvirt_volume.vm_q2_resized.id : null

  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  console {
    type        = "pty"
    target_type = "virtio"
    target_port = "1"
  }

  graphics {
    type = "spice"
    listen_type = "address"
    autoport = true
  }
}
