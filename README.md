# Libvirt cloud-init Terraform Module

Simple libvirt cloud-init module.

## Example Usage

This will create two VMs from the provided img/qcow2 image passed in `Q2_SOURCE`.

```tf
variable SSH_AUTH_KEY {
  description = "SSH Key"
  default = "ssh....."
}

variable test_inst {
  description = ""
  type = map
  default = {
    ubu20 = {
      NAME = "ubuntu20"
      Q2_SOURCE = "/path/to/ubuntu20/img"
    },
    ubuntu22 = {
      NAME = "ubuntu22"
      Q2_SOURCE = "/path/to/ubuntu22/img"
    }
  }
}

module "libvirt_cloud_init" {
  source = "git::https://gitlab.com/c2-games/infrastructure/terraform/libvirt-cloud-init.git?ref=main"

  for_each = var.test_inst
  NAME = each.value.NAME
  Q2_SOURCE = each.value.Q2_SOURCE
  SSH_AUTH_KEY = var.SSH_AUTH_KEY
}
```

## Variable Overview

This section will explain variables present in `variables.tf`.

### VM variables

- `NAME`
  - Name of the VM displayed in libvirt
- `FIRMWARE`
  - Firmware overwrite if using UEFI
- `DISK_SIZE`
  - Disk size in bytes
  - Default: `8589934592`
- `VM_MEMORY`
  - VM memory in bytes
  - Default: `2048`
- `VM_VCPU`
  - Number of vCPUs given to VM
  - Default: `2`
- `VM_CPU_MODE`
  - Sets CPU mode on VM
  - Default: `host-passthrough`
- `VM_NETWORK`
  - Sets VM network interface
  - Default: `default`
- `VM_NETWORK_WAIT_FOR_LEASE`
  - Wait until network interface gets DHCP address
  - Default: `true`
- `KERNEL_OVERWRITE`
  - Defines if direct kernel boot is utilized
  - Default: `false`

### Cloud-init variables

- `USERNAME`
  - Username provisioned within VM
  - Default: `cloud-user`
- `SSH_AUTH_KEY`
  - SSH key set in ${USERNAME} authorized keys
- `Q2_SOURCE`
  - qcow2/img used to build VM

