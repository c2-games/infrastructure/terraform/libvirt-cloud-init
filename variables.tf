######
# vm variables
######

variable "NAME" {
  description = "Name of created VM"
  type = string
  default = "cloud-init-vm"
}

variable "FIRMWARE" {
  description = "Optional firmware overwrite"
  type = string
  default = null
}

variable "DISK_SIZE" {
  description = "Disk size in bytes"
  type = number
  default = 8589934592
  # 32GB - 32212254720
  # 16GB - 17179869184
  # 8GB - 8589934592
}

variable "VM_MEMORY" {
  description = "RAM size in bytes"
  type = number
  default = 2048
}

variable "VM_VCPU" {
  description = "Number of VCPUs"
  type = number
  default = 2
}

variable "VM_CPU_MODE" {
  description = "CPU mode"
  type = string
  default = "host-passthrough"
}

variable "VM_NETWORK" {
  description = "Network VM is attached to"
  type = string
  default = "default"
}

variable "VM_NETWORK_WAIT_FOR_LEASE" {
  description = "Toggle to wait for network lease"
  type = bool
  default = true
}

variable "KERNEL_OVERWRITE" {
  description = "Toggle to overwrite kernel ref"
  type = bool
  default = false
}

######
# cloud-init variables
######

variable "USERNAME" {
  description = "VM username"
  type = string
  default = "cloud-user"
}

variable "SSH_AUTH_KEY" {
  description = "SSH authorized key"
  type = string
  default = null
}

variable "Q2_SOURCE" {
  description = "Full path to qcow2 image"
  type = string
  default = null
}

